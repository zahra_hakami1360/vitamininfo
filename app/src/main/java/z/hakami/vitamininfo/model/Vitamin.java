package z.hakami.vitamininfo.model;

import java.io.Serializable;

public class Vitamin implements Serializable {
    private String name;
    private String source;
    private String importance;
    private String deficiencySyndrome;
    private String dailyDose;
    private String note ;
    private String overDoseSyndrome;

    public Vitamin(String name, String source, String importance, String deficiencySyndrome, String dailyDose, String note, String overDoseSyndrome) {
        this.name = name;
        this.source = source;
        this.importance = importance;
        this.deficiencySyndrome = deficiencySyndrome;
        this.dailyDose = dailyDose;
        this.note = note;
        this.overDoseSyndrome = overDoseSyndrome;
    }

    public String getName() {
        return name;
    }

    public String getSource() {
        return source;
    }

    public String getImportance() {
        return importance;
    }

    public String getDeficiencySyndrome() {
        return deficiencySyndrome;
    }

    public String getDailyDose() {
        return dailyDose;
    }

    public String getNote() {
        return note;
    }

    public String getOverDoseSyndrome() {
        return overDoseSyndrome;
    }

    @Override
    public String toString() {
        return
                name;
    }
   /** @Override
    public int compareTo(Vitamin v) {
        //Soll das übergebenen Objekt v untergeordnet werden return -1
        //Soll das übergebene Objekt v übergeordnet werden dann 1
        if (this.getName() == null && v.getName() == null) {
            return 0;
        }
        if (this.getName() == null) {
            return 1;
        }
        if (v.getName() == null) {
            return -1;
        }
        return this.getName().compareTo(v.getName());
    }*/
}
