package z.hakami.vitamininfo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import z.hakami.vitamininfo.model.Vitamin;


public class ListVitaminActivity extends AppCompatActivity {

    private List<Vitamin> vitaminsList = new LinkedList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setListViewWithVitamines();

        //Lister OnItemClickListener implementieren
        //Die listView bekommt einen Listener vom Typ OnItemClickListener
        //--> das ist ein funktionale Interface mit METHODE methode OnItemClick(AdapterView parent, View view, int position(index des Items))
        //funktionales Interface erlaubt lambda aufruf
         ListView listViewVitamins = findViewById(R.id.ListViewCsv);
        listViewVitamins.setOnItemClickListener((parent, view, position, id) -> showDetail(parent,view,position));
    }

    public void setListViewWithVitamines() {

        ListView listViewVitamin = findViewById(R.id.ListViewCsv);
        vitaminsList = getListVitaminFromCsv(R.raw.vitamins);
        ArrayAdapter<Vitamin> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, vitaminsList);
        listViewVitamin.setAdapter(adapter);
        // CSV Datei auslesen

        //Zeile für Zeile auslesen, Objekterzeugen, Objekt in Collection LiskedList = listVitamine;
        //Die Liste mit dem Element ListView verbinden
    }

    public List<Vitamin> getListVitaminFromCsv(int resId) {
        // Öffnung zur CSV Datei über einen ByteStream
        // ergänzt um eine CharacterReader (Bufferedreader)
        // Dieser benötig um auf die Datei zuzugreifen den Inputstream und nutzt dafür den InputStreamreader

        try (InputStream inputStream = getResources().openRawResource(resId);
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {

            String csvRow;
            while ((csvRow = reader.readLine()) != null) {
                Log.i("Output CSV", csvRow);
                String[] VitaminData = csvRow.split(";");
                Log.i("Output CSV zeile als Array", Arrays.toString(VitaminData));

                //Vitamin erstellen aus VitaminData
                Vitamin vitamin = new Vitamin(VitaminData[0], VitaminData[1], VitaminData[2], VitaminData[3], VitaminData[4], VitaminData[5], VitaminData[6]);

                vitaminsList.add(vitamin);
                Log.i("Output CSV zeile als Array", Arrays.toString(VitaminData));
            }
            return vitaminsList;
        } catch (IOException e) {
            Log.e("IO_Exception", e.getMessage());
            // das seight uns alle fäller
            e.printStackTrace();
            return null;
        }
    }
    public void showDetail(AdapterView<?>parent, View view,int position){
        Vitamin vitamin =(Vitamin)parent.getAdapter().getItem(position);
        Log.i("output ",vitamin.toString());

        Intent intent = new Intent(this,DetailActivity.class);
        //putExtra benötigt Schlüssel Wert paar
        //um Objekte zu übergeben müssen diese das Interface Serializable (Markerinterface Serialisierbar) sein
        intent.putExtra("Vitamin",vitamin);
        startActivity(intent);
    }
}