package z.hakami.vitamininfo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import z.hakami.vitamininfo.model.Vitamin;

public class DetailActivity extends AppCompatActivity {
    private Vitamin vitamin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        showView();
        //Alle TextViewelemente holen
        //Text reinsetzen

    }
    public void showView(){
        //Annahme des aufrufenden Intents
        Intent intent = getIntent();
        if(intent != null){
            if(intent.hasExtra("Vitamin")){
                vitamin = (Vitamin) intent.getSerializableExtra("Vitamin");
            }
        }




        getTextViewById(R.id.textViewVitaminname).setText(vitamin.getName());


        getTextViewById(R.id.textView_lblSource).setText(vitamin.getSource());
        getTextViewById(R.id.textViewVitaminname).setText(getString(R.string.sourc));

        getTextViewById(R.id.textView_lblImportance).setText(vitamin.getImportance());
        getTextViewById(R.id.textViewContentSource).setText(getString(R.string.Importance));

        getTextViewById(R.id.textView_lblDeficiencySyndrome).setText(vitamin.getDeficiencySyndrome());
        getTextViewById(R.id.textViewContentdeficiencySyndrome).setText(getString(R.string.DeficiencySyndrome));

        getTextViewById(R.id.textView_lblDailyDose).setText(vitamin.getDailyDose());
        getTextViewById(R.id.textViewContantDailyDose).setText(getString(R.string.DailyDose));

        getTextViewById(R.id.textView_lblNote).setText(vitamin.getNote());
        getTextViewById(R.id.textViewContantNote).setText(getString(R.string.Note));

        getTextViewById(R.id.textView_lblOverDoseSyndrome).setText(vitamin.getOverDoseSyndrome());
        getTextViewById(R.id.textViewContantNote).setText(getString(R.string.OverDoseSyndrome));

        getTextViewById(R.id.textViewAdvice).setText(getString(R.string.Advice));

    }
    public TextView getTextViewById(int resId){
        //Todo Prüfung ob eine TextView zurückkommt
        return findViewById(resId);
    }

    public void goBacktoIntentActivity(View view){

        finish();
    }
}